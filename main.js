// Задание:
// 1. Реализуем приложение Список дел с функциональностью как на практике. (Возможность добавить новый элемент в список из инпута по нажатию кнопки "Добавить");
// 2. Реализовать удаление элемента по нажатию крестика;
// 3. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.
// 4. Реализовать возможность редактирования элемента. 
// - При нажатии на текст элемента он отображается в инпуте, название кнопки меняется на "Обновить". 
// - После изменения названия и нажатия на "Обновить" текст элемента изменяется в списке, инпут сбрасывается, кнопка переименовывается обратно в "Добавить". 
// - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить". 
// - Редактируемый элемент подсвечивается как активный.

const inputBtnEl = document.querySelector('.js-input-group button');
const listEl = document.querySelector('.js-list');
const inputEl = document.querySelector('.js-input');

const switchBtnStatus = mode => {
    if (mode === 'add') {
        inputBtnEl.classList.add('btn-primary');
        inputBtnEl.classList.remove('btn-warning');
        inputBtnEl.textContent = 'Добавить';
        inputBtnEl.setAttribute('mode', 'add');
    }
    if (mode === 'edit') {
        inputBtnEl.classList.remove('btn-primary');
        inputBtnEl.classList.add('btn-warning');
        inputBtnEl.textContent = 'Редактировать';
        inputBtnEl.setAttribute('mode', 'edit');
    }
};

//Клик по кнопке Добавить/Редактировать
inputBtnEl.addEventListener(
    'click',

    (e) => {
        
        const mode = inputBtnEl.getAttribute('mode');
        
        if (mode === 'add') {
            if (!inputEl.value) {
                return;
            }
            const inputText = inputEl.value;
            inputEl.value = '';
            const newItem = document.createElement('li');
            newItem.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-start');
            newItem.innerHTML = `
                            <div class="ms-2 me-auto">    
                                <input type="checkbox" class="js-list-item-checkbox">    
                                <span class="js-list-item-text">${inputText}</span>
                            </div>    
                            <span class="badge bg-danger rounded-pill js-delete-btn">x</span>`;
            listEl.appendChild(newItem);
        }
        
        if (mode === 'edit') {

/*             if (inputBtnEl.getAttribute('mode') !== 'edit') {
                return;
            } */
            const inputText = inputEl.value;
            inputEl.value = '';
            document.querySelector('.js-list-item-text__active').textContent = inputText;                
            document.querySelector('.js-list-item-text__active').parentNode.parentNode.removeAttribute('id');
            document.querySelector('.js-list-item-text__active').classList.remove('js-list-item-text__active');
            document.querySelector('.js-input-group span').setAttribute('hidden', 'hidden');
            switchBtnStatus('add');
        }
    }
);

//Зачеркивание текста по чек-боксу
listEl.addEventListener(
    'click',

    e => {
        if (!e.target.classList.contains('js-list-item-checkbox')) {
            return;
        }
        e.target.parentNode.classList.toggle('line-through');
    }
);

//Удаление элемента списка
listEl.addEventListener(
    'click',

    e => {
        if (!e.target.classList.contains('js-delete-btn')) {
            return;
        }
        e.target.parentNode.remove();
    }
);


// Редактирование элемента списка
listEl.addEventListener(
    'click',

    e => {
        if (!e.target.classList.contains('js-list-item-text')) {
            return;
        }
        // Блокирование нового редактирования, если какой-то элемент уже редактируется:
        //for (i = 0; i < document.querySelectorAll('.js-list li').length; i++) {
        //    if (!document.querySelectorAll('.js-list li')[i].hasAttribute('id')) {
        //        continue;
        //    }
        //    return;
        //}
        for (i = 0; i < document.querySelectorAll('.js-list li').length; i++) {
            document.querySelectorAll('.js-list li')[i].removeAttribute('id')
        }
        e.target.classList.add('js-list-item-text__active');
        inputEl.value = e.target.textContent;
        e.target.parentNode.parentNode.setAttribute('id', 'active-item');
        switchBtnStatus('edit');
        document.querySelector('.js-quit-edit-btn').removeAttribute('hidden');
        document.querySelector('.js-quit-edit-btn').addEventListener( // Отмена редактирования
            'click',
            () => {
                switchBtnStatus('add');
                inputEl.value = '';                
                document.querySelector('.js-list-item-text__active').parentNode.parentNode.removeAttribute('id');
                document.querySelector('.js-quit-edit-btn').setAttribute('hidden','hidden');
                document.querySelector('.js-list-item-text__active').classList.remove('js-list-item-text__active');
            }
        );
    }
);
